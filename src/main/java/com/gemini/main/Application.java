package com.gemini.main;

import com.alicloud.openservices.tablestore.ClientException;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

public class Application {

    private static final String sms_endpoint = "dysmsapi.aliyuncs.com";

    private static final String sendSms_action = "SendSms";

    private static final String signName_CN = "启飞智能";

    private static final String RegionId = "cn-hangzhou";

    private static DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAIuufvAy6U56Gf", "WAScPW36KF7owIKPucmYpDhzv2QznH");

    private static IAcsClient client = new DefaultAcsClient(profile);

    public static void main(String[] args) throws ClientException, com.aliyuncs.exceptions.ClientException {
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain(sms_endpoint);
        request.setVersion("2017-05-25");
        request.setAction(sendSms_action);
        request.putQueryParameter("RegionId", RegionId);
        request.putQueryParameter("PhoneNumbers", "18656432052");
        request.putQueryParameter("SignName", signName_CN);
        request.putQueryParameter("TemplateCode", "SMS_137160107");
        request.putQueryParameter("TemplateParam", "{\"code\":\"878689\"}");
        CommonResponse response = client.getCommonResponse(request);
        System.out.println(response.getData());
    }

}
