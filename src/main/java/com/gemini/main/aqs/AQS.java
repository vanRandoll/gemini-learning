package com.gemini.main.aqs;

import java.util.concurrent.locks.AbstractOwnableSynchronizer;

public abstract class AQS extends AbstractOwnableSynchronizer
        implements java.io.Serializable {
    private static final long serialVersionUID = 6902539419542272200L;

    static final class Node {
        // 取值为上面的1、-1、-2、-3，或者0(以后会讲到)
        // 这么理解，暂时只需要知道如果这个值 大于0 代表此线程取消了等待，
        //    ps: 半天抢不到锁，不抢了，ReentrantLock是可以指定timeouot的。。。
        volatile int waitStatus;
        // 前驱节点的引用
        volatile Node prev;
        // 后继节点的引用
        volatile Node next;
        // 这个就是线程本尊
        volatile Thread thread;
    }

    public final void acquire(int arg) {
    }

    protected boolean tryAcquire(int arg) {
        throw new UnsupportedOperationException();
    }
}
