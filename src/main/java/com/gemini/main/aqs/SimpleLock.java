package com.gemini.main.aqs;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class SimpleLock implements Lock, java.io.Serializable {
    private static final long serialVersionUID = -415889179929889938L;

    /** Synchronizer providing all implementation mechanics */
    private final Sync sync;

    SimpleLock() {
        sync = new FairSync();
    }


    abstract static class Sync extends AQS {

        private static final long serialVersionUID = 554671456659672214L;

        abstract void lock();
    }

    @Override
    public void lock() {
        sync.lock();
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {

    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public void unlock() {

    }

    @Override
    public Condition newCondition() {
        return null;
    }

    static final class FairSync extends Sync {

        private static final long serialVersionUID = -8607850198775667328L;

        final void lock() {
            acquire(1);
        }

    }
}
