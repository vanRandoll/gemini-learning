package com.gemini.main.assign;

import java.util.Random;

/**
 * gemini
 * com.gemini.main.assign.LiteralTest
 *
 * @author zhanghailin
 * 静态分派发生在编译阶段，因此确定静态分派的动作实际上不是由虚拟机来执行的，而是由编译器来完成。
 * <p>
 * 但是，字面量没有显示的静态类型，它的静态类型只能通过语言上的规则去理解和推断
 * <p>
 * *******将重载方法从上向下依次注释，将会得到不同的输出**************
 */
public class LiteralTest {

    public static void sayHello(String arg) {//新增重载方法
        System.out.println("hello String");
    }


    public static void sayHello(char arg){
        System.out.println("hello char");
    }
    public static void sayHello(int arg) {
        System.out.println("hello int");
    }

    public static void sayHello(long arg) {
        System.out.println("hello long");
    }

    public static void sayHello(Character arg) {
        System.out.println("hello Character");
    }

    public static void main(String[] args) {
        sayHello('a');
        Random r = new Random();
        String s = "abc";
        int i = 0;
        //如果编译器无法确定要自定转型为哪种类型，会提示类型模糊，拒绝编译
        //sayHello(r.nextInt() % 2 != 0 ? s : i);//编译错误
        //sayHello(r.nextInt() % 2 != 0 ? 'a' : false);//编译错误

    }

}
