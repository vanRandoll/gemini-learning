package com.gemini.main.assign;

/**
 * gemini
 * com.gemini.main.assign.StaticAssign
 *
 * @author zhanghailin
 */
public class StaticAssign {

    static abstract class Human {
    }

    static class Man extends Human {
    }

    static class Woman extends Human {
    }

    public static void sayHello(Human guy) {
        System.out.println("hello,guy!");
    }

    public static void sayHello(Man guy) {
        System.out.println("hello,gentlemen!");
    }

    public static void sayHello(Woman guy) {
        System.out.println("hello,lady!");
    }

    // 静态类型编译期可知，实际类型运行期可知
    // 编译器在重载时是通过参数的静态类型而不是实际类型作为判定的依据。
    // 并且静态类型在编译期可知，因此，编译阶段，Javac编译器会根据参数的静态类型决定使用哪个重载版本。
    //
    // 所有依赖静态类型来定位方法执行版本的分派动作称为静态分派。静态分派的典型应用就是【方法重载】
    public static void main(String[] args) {
        Human man = new Man();
        Human woman = new Woman();
        sayHello(man);
        sayHello(woman);
        sayHello((Man) man);//类型转换，静态类型变化，我们知道转型后的静态类型一定是Man
        man = new Woman(); //实际类型变化,实际类型却是不确定的
        sayHello((Woman) man);//类型转换，静态类型变化
    }

}
