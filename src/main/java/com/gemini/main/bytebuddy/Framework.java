package com.gemini.main.bytebuddy;

/**
 * gemini
 * com.gemini.main.bytebuddy.Framework
 *
 * @author zhanghailin
 */
public interface Framework {

    <T> T secure(Class<T> type);
}
