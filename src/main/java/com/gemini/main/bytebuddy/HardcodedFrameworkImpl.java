package com.gemini.main.bytebuddy;

/**
 * gemini
 * com.gemini.main.bytebuddy.HardcodedFrameworkImpl
 *
 * @author zhanghailin
 */
public class HardcodedFrameworkImpl implements Framework {
    @Override
    public <T> T secure(Class<T> type) {
        if(type == Service.class) {
            return (T) new SecuredService();
        } else {
            throw new IllegalArgumentException("Unknown: " + type);
        }
    }
}
