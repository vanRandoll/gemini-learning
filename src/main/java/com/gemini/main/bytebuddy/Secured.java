package com.gemini.main.bytebuddy;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * gemini
 * com.gemini.main.bytebuddy.Secured
 *
 * @author zhanghailin
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Secured {

    String user();
}
