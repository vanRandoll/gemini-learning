package com.gemini.main.bytebuddy;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.agent.ByteBuddyAgent;
import net.bytebuddy.dynamic.loading.ClassReloadingStrategy;

/**
 * gemini
 * com.gemini.main.bytebuddy.SecuredService
 *
 * @author zhanghailin
 */
public class SecuredService extends Service {

    @Override
    void deleteEverything() {
        if(UserHolder.user.equals("ADMIN")) {
            super.deleteEverything();
        } else {
            throw new IllegalStateException("Not authorized");
        }
    }

    public static void main(String[] args) {
        ByteBuddyAgent.install();
        Foo foo = new Foo();
        new ByteBuddy()
                .redefine(Bar.class)
                .name(Foo.class.getName())
                .make()
                .load(Foo.class.getClassLoader(), ClassReloadingStrategy.fromInstalledAgent());
        System.out.println(foo.m());
    }
}

class Foo {
    String m() { return "foo"; }
}

class Bar {
    String m() { return "bar"; }
}
