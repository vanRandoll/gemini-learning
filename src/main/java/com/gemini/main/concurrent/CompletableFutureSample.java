package com.gemini.main.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CompletableFutureSample {


    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();





        executorService.execute(new Run1());

        TimeUnit.SECONDS.sleep(5);
        executorService.execute(() -> {
            System.out.println(Thread.currentThread().getName()+":------------11111111111--------------");

        });
    }

    static class Run1 implements Runnable {
        @Override
        public void run() {
            int count = 0;
            while (true) {
                count ++;
                System.out.println(Thread.currentThread().getName()+":------------2222222222222--------------");
                if (count ==10) {
                    System.out.println(1/0);
                }
                if (count == 20) {
                    System.out.println(Thread.currentThread().getName()+"counter:"+count);
                    break;
                }
            }
        }
    }
}
