package com.gemini.main.concurrent;

public interface Computable<A, V> {

    V compute(A arg) throws InterruptedException;
}
