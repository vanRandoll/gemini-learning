package com.gemini.main.concurrent;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionBoundedBuffer<T> {

    protected final Lock lock = new ReentrantLock();

    // 条件谓词 队列不满 counter < items.length
    private final Condition notFull = lock.newCondition();

    // 条件谓词 不为空 counter > 0
    private final Condition notEmpty = lock.newCondition();

    private final T[] items;

    private int head = 0, tail = 0, count = 0;

    public ConditionBoundedBuffer(int capacity) {
        items = (T[]) new Object[capacity];
    }

    public void put(T t) throws InterruptedException {
        lock.lock();
        try {
            while (count == items.length)
                notFull.await();
            items[tail] = t;
            if (++tail == items.length)
                tail = 0;
            ++count;
            notEmpty.signal();
        } finally {
            lock.unlock();
        }

    }

    public T take() throws InterruptedException {
        lock.lock();
        T t;
        try {
            while (count == 0)
                notEmpty.await();
            t = items[head];
            items[head] = null;// help GC
            if (++head == items.length)
                head = 0;
            --count;
            notFull.signal();
            return t;
        } finally {
            lock.unlock();
        }
    }


}
