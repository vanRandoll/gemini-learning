package com.gemini.main.concurrent;

public interface Lock {

    void lock();

    void unlock();
}
