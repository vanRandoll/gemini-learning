package com.gemini.main.concurrent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Memoizer<A, V> implements Computable<A, V> {


    private final Map<A, V> cache = new HashMap<A, V>();
    private final Computable<A, V> c;

    public Memoizer(Computable<A, V> c) {
        this.c = c;
    }

    @Override
    public V compute(A arg) throws InterruptedException {
        V result = cache.get(arg);
        if (result == null) {
            result = c.compute(arg);
            cache.put(arg, result);
        }
        return result;
    }

    public static void main(String[] args) {
        /*int t=1,p=2,tail =3,head =4,q=5;
        //p = (t != (t = tail)) ? t : head;
        p = (p != t && t != (t = tail)) ? t : q;
        System.out.println("t:"+t);
        System.out.println("p:"+p);
        System.out.println("tail:"+tail);
        System.out.println("head:"+head);*/

        ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<>();
        Thread a = new Thread(() -> {
           for(int i = 0;i<5;i++) {
               queue.offer(i);
           }
        });
        a.start();
    }
}
