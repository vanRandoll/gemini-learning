package com.gemini.main.concurrent;


import java.util.concurrent.CountDownLatch;

/**
 * 闭锁的简单案例
 */
public class TestHarness {

    public long timeTasks(int nThreads, final Runnable task) throws InterruptedException {
        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(nThreads);
        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(() -> {
                try {
                    try {
                        startGate.await();
                    } finally {
                        endGate.countDown();
                    }
                } catch (InterruptedException e) {

                }
            });
            t.start();
        }
        long start = System.nanoTime();
        startGate.countDown();
        endGate.await();
        long end = System.nanoTime();
        return end - start;

    }
}
