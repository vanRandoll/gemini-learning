package com.gemini.main.decorate;

/**
 * gemini
 * com.gemini.main.decorate.Beverage
 *
 * @author zhanghailin
 */
public abstract class Beverage {

    String description = "unknown beverage";

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}
