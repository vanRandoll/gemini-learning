package com.gemini.main.decorate;

/**
 * gemini
 * com.gemini.main.decorate.CondimentDecorate
 *
 * @author zhanghailin
 */
public abstract class CondimentDecorate extends Beverage {

    public abstract String getDescription();
}
