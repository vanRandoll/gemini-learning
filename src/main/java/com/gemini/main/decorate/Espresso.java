package com.gemini.main.decorate;

/**
 * gemini
 * com.gemini.main.decorate.Espresso
 *
 * @author zhanghailin
 */
public class Espresso extends Beverage {

    public Espresso() {
        description = "Espresso";
    }

    @Override
    public double cost() {
        return 1.99;
    }
}
