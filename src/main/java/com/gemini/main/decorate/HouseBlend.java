package com.gemini.main.decorate;

/**
 * gemini
 * com.gemini.main.decorate.HouseBlend
 *
 * @author zhanghailin
 */
public class HouseBlend extends Beverage {

    public HouseBlend() {
        description = "House blend beverage";
    }

    @Override
    public double cost() {
        return 0.89;
    }
}
