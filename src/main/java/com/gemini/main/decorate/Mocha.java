package com.gemini.main.decorate;

/**
 * gemini
 * com.gemini.main.decorate.Mocha
 *
 * @author zhanghailin
 */
public class Mocha extends CondimentDecorate {

    Beverage beverage;

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ",Mocha";
    }

    @Override
    public double cost() {
        return 0.20 + beverage.cost();
    }
}
