package com.gemini.main.designpattern.template;

public abstract class AbstractTemplate {

    public void templateMethod() {

    }

    protected void init() {
        System.out.println("init 抽象层已经实现，子类也可以复写");
    }

    protected abstract void apply();

    protected void end() {

    }
}
