package com.gemini.main.disruptor.event;

/**
 * gemini
 * com.gemini.main.disruptor.event.LongEvent
 *
 * @author zhanghailin
 */
public class LongEvent {

    private long value;

    public void setValue(long value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "LongEvent{" +
                "value=" + value +
                '}';
    }
}
