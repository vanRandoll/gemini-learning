package com.gemini.main.disruptor.event;

import com.lmax.disruptor.EventFactory;

/**
 * gemini
 * com.gemini.main.disruptor.event.LongEventFactory
 *
 * @author zhanghailin
 */
public class LongEventFactory implements EventFactory<LongEvent> {
    @Override
    public LongEvent newInstance() {
        return new LongEvent();
    }
}
