package com.gemini.main.disruptor.event;

import com.lmax.disruptor.EventHandler;

/**
 * gemini
 * com.gemini.main.disruptor.event.LongEventHandler
 *
 * @author zhanghailin
 */
public class LongEventHandler implements EventHandler<LongEvent> {
    @Override
    public void onEvent(LongEvent event, long sequence, boolean endOfBatch) throws Exception {
        System.out.println("Event: " + event);
    }
}
