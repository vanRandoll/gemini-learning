package com.gemini.main.i18n;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Test {

    public static void main(String[] args) {

        Calendar calendar = Calendar.getInstance();

        System.out.println(calendar.getTimeInMillis());
        System.out.println(calendar.getTimeZone());
    }
}
