package com.gemini.main.jraft.counter;

/**
 * gemini
 * com.gemini.main.jraft.counter.CounterService
 *
 * @author zhanghailin
 */
public interface CounterService {

    void get(final boolean readOnlySafe, final CounterClosure closure);

    void incrementAndGet(final long delta, final CounterClosure closure);
}
