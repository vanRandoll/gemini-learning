package com.gemini.main.jraft.counter.rpc;

import com.alipay.remoting.AsyncContext;
import com.alipay.remoting.BizContext;
import com.alipay.remoting.rpc.protocol.AsyncUserProcessor;
import com.alipay.sofa.jraft.Status;
import com.gemini.main.jraft.counter.CounterClosure;
import com.gemini.main.jraft.counter.CounterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * gemini
 * com.gemini.main.jraft.counter.rpc.GetValueRequestProcessor
 *
 * @author zhanghailin
 */
public class GetValueRequestProcessor extends AsyncUserProcessor<GetValueRequest> {

    private static final Logger LOG = LoggerFactory.getLogger(GetValueRequestProcessor.class);

    private final CounterService counterService;

    public GetValueRequestProcessor(CounterService counterService) {
        super();
        this.counterService = counterService;
    }

    /**
     * 这是一个异步的模式
     *
     * @param bizContext
     * @param asyncContext
     * @param getValueRequest
     */
    @Override
    public void handleRequest(BizContext bizContext, AsyncContext asyncContext, GetValueRequest getValueRequest) {
        final CounterClosure closure = new CounterClosure() {
            @Override
            public void run(Status status) {
                asyncContext.sendResponse(getValueResponse());
            }
        };

        this.counterService.get(getValueRequest.isReadOnlySafe(), closure);
    }

    @Override
    public String interest() {
        return GetValueRequestProcessor.class.getName();
    }
}
