package com.gemini.main.jvm;

import java.io.IOException;
import java.io.InputStream;

public class ClassLoadTest {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        ClassLoader classLoader = new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                try {
                    String filename = name.substring(name.lastIndexOf(".") + 1) + ".class";

                    InputStream inputStream = getClass().getResourceAsStream(filename);
                    if (inputStream == null)
                        return super.loadClass(name);
                    byte[] b = new byte[inputStream.available()];
                    inputStream.read(b);
                    return defineClass(name, b, 0, b.length);
                } catch (IOException e) {
                    throw new ClassNotFoundException(name);
                }
            }
        };

        Object obj = classLoader.loadClass("com.gemini.main.jvm.ClassLoadTest").newInstance();

        System.out.println(obj.getClass());
        System.out.println(obj instanceof com.gemini.main.jvm.ClassLoadTest);
    }
}
