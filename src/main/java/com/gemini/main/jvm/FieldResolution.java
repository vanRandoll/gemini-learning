package com.gemini.main.jvm;


public class FieldResolution {

    //<clinit>() 方法是有编译器自动收集类中的所有类变量的赋值动作和静态语句块中国的语句合并并产生的
    // 编译器收集的顺序是由语句在源文件中刚出现的顺序锁决定的，静态语句块中只能访问到定义在静态语句块之前的变量
    // 定义在它之后的变量，在前面的静态语句块中可以复制，但是不能访问
    static {
        i = 0;
        //System.out.println(i);
    }

    static int i = 1;

    interface Interface0 {
        int A = 0;
    }

    interface Interface1 extends Interface0 {
        int A = 1;
    }

    interface Interface2 {
        int A = 2;
    }

    static class Parent implements Interface1 {
        public static int A = 3;
    }

    static class Sub extends Parent implements Interface2 {
        // 注释掉此行，将无法编译
        public static int A = 4;
    }

    static class DeadLoopClass {
        static {
            if (true) {
                System.out.println(Thread.currentThread()+" init DeadLoopClass");
                while (true) {}
            }
        }
    }

    public static void main(String[] args) {
        //System.out.println(i);
        Runnable script = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread()+" start");
                DeadLoopClass dlc = new DeadLoopClass();
                System.out.println(Thread.currentThread()+ " run over");
            }
        };

        Thread thread1 = new Thread(script);
        Thread thread2 = new Thread(script);

        thread1.start();
        thread2.start();
    }
}
