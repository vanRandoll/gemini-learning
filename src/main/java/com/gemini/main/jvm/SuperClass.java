package com.gemini.main.jvm;

public class SuperClass {

    static {
        System.out.println("super class init...");
    }

    public static int a = 1;
}
