package com.gemini.main.leetcode;

import java.util.Arrays;

/**
 * gemini
 * com.gemini.main.leetcode.Buddy
 *
 * @author zhanghailin
 */
public class Buddy {

    public static void main(String[] args) {
        int maxOrder = 11;
        int maxSubpageAllocs = 1 << maxOrder;

        // Generate the memory map.
        byte[] memoryMap = new byte[maxSubpageAllocs << 1];
        byte[] depthMap = new byte[memoryMap.length];
        int memoryMapIndex = 1;
        for (int d = 0; d <= maxOrder; ++ d) { // move down the tree one level at a time
            int depth = 1 << d;
            for (int p = 0; p < depth; ++ p) {
                // in each level traverse left to right and set value to the depth of subtree
                memoryMap[memoryMapIndex] = (byte) d;
                depthMap[memoryMapIndex] = (byte) d;
                memoryMapIndex ++;
            }
        }
        System.out.println(memoryMap.length);
        System.out.println(depthMap.length);

        System.out.println(Arrays.toString(memoryMap));
        System.out.println(Arrays.toString(depthMap));
    }
}
