package com.gemini.main.link;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * gemini
 * com.gemini.main.link.Link
 *
 * @author zhanghailin
 */
public class Link {

    private Node link = null;

    private Node first = null;

    public void insert(Node node) {
        if (link == null) {
            link = node;
            first = node;
            return;
        }
        if (node.k < first.k) {
            first = node;
        }
        Node n = link;
        for (; ; ) {
            if (n.k <= node.k) {
                if (n.next == null) {
                    n.next = node;
                    node.pre = n;
                    return;
                }
                n = n.next;
            } else {
                if (n.pre == null) {
                    n.pre = node;
                    node.next = n;
                    return;
                }
                n = n.pre;
            }
        }



    }


    public static class Node {
        private int k;
        private List<String> list;

        private Node pre;
        private Node next;

        Node(int k, List<String> list) {
            this.k = k;
            this.list = list;
        }
    }


    public static void main(String[] args) {
        Link link = new Link();
        link.insert(new Node(3,new ArrayList<String>(Collections.singletonList("3"))));
        link.insert(new Node(0,new ArrayList<String>(Collections.singletonList("0"))));
        link.insert(new Node(0,new ArrayList<String>(Collections.singletonList("1"))));
        System.out.println(link.first.list.toString());
        System.out.println(link.first.next.list.toString());
        System.out.println(link.first.next.next.list.toString());
    }

}
