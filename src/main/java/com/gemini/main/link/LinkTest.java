package com.gemini.main.link;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * gemini
 * com.gemini.main.link.LinkTest
 *
 * @author zhanghailin
 */
public class LinkTest {

    public static void main(String[] args) {
        Map<Integer,String> a = new LinkedHashMap<>();
        a.put(5,"a");
        a.put(5,"b");
        a.put(5,"c");
        System.out.println(a.toString());
    }
}
