package com.gemini.main.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * gemini
 * com.gemini.main.log.Log4j2Test
 *
 * @author zhanghailin
 */
public class Log4j2Test {

    //通过名字获取logger
    private static final Logger consoleLogger = LogManager.getLogger("console_appender");
    private static final Logger fileLogger = LogManager.getLogger("file_appender");
    public static void main(String[] args) {
        //consoleLogger.info("Hello World console");
        fileLogger.info("Hello World file");
    }
}
