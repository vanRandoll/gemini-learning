package com.gemini.main.nio;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public interface ServerSocketHandler {

    void handleAccept(SelectionKey selectionKey) throws IOException;

    String handleRead(SelectionKey selectionKey) throws IOException;
}
