package com.gemini.main.proxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * gemini
 * com.gemini.main.proxy.CglibTest
 *
 * @author zhanghailin
 */
public class CglibTest implements MethodInterceptor {

    /**
     * 生成Cglib代理对象
     * @param cla 真实对象的class对象
     * @return Cglib代理对象
     */
    public Object getProxy(Class cla){
        //CGLIB的增强类对象
        Enhancer enhancer=new Enhancer();
        //设置增强类型，增强真实对象
        enhancer.setSuperclass(cla);
        //定义代理逻辑对象为当前对象，要求当前对象实现MethodInterceptor中的抽象方法。
        enhancer.setCallback(this);
        return enhancer.create();

    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("调用真实对象前");
        Object result= methodProxy.invokeSuper(o, objects);
        System.out.println("调用真实对象后");

        return result;
    }

    public static void main(String[] args) {

    }
}
