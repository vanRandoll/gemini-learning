package com.gemini.main.proxy;

public interface NeedProxyInterface {

    void doSomething();
}
