package com.gemini.main.proxy;

import javassist.util.proxy.ProxyObject;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * gemini
 * com.gemini.main.proxy.ProxyEnum
 *
 * @author zhanghailin
 */
@SuppressWarnings("unchecked")
public enum ProxyEnum {

    JDK_PROXY(new ProxyFactory() {

        public <T> T newProxyInstance(Class<T> interfaceClass, Object handler) {
            return interfaceClass.cast(Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{interfaceClass}, (InvocationHandler) handler));
        }
    }),
    BYTE_BUDDY_PROXY(new ProxyFactory() {
        public <T> T newProxyInstance(Class<T> interfaceClass, Object handler) throws IllegalAccessException, InstantiationException {
            Class<? extends T> cls = new ByteBuddy()
                    .subclass(interfaceClass)
                    .method(ElementMatchers.isDeclaredBy(interfaceClass))
                    .intercept(MethodDelegation.to(handler, "handler"))
                    .make()
                    .load(interfaceClass.getClassLoader(), ClassLoadingStrategy.Default.INJECTION)
                    .getLoaded();

            return  cls.newInstance();
        }
    }),
    CGLIB_PROXY(new ProxyFactory() {
        public <T> T newProxyInstance(Class<T> interfaceClass, Object handler) {
            Enhancer enhancer = new Enhancer();
            enhancer.setCallback((MethodInterceptor) handler);
            enhancer.setInterfaces(new Class[]{interfaceClass});
            return (T) enhancer.create();
        }
    }),

    JAVASSIST_BYTECODE_PROXY(new ProxyFactory() {
        public <T> T newProxyInstance(Class<T> interfaceClass, Object handler) {
            return (T) com.alibaba.dubbo.common.bytecode.Proxy.getProxy(interfaceClass).newInstance((InvocationHandler) handler);
        }
    }),

    JAVASSIST_DYNAMIC_PROXY(new ProxyFactory() {
        public <T> T newProxyInstance(Class<T> interfaceClass, Object handler) throws IllegalAccessException, InstantiationException {
            javassist.util.proxy.ProxyFactory proxyFactory = new javassist.util.proxy.ProxyFactory();
            proxyFactory.setInterfaces(new Class[]{interfaceClass});
            Class<?> proxyClass = proxyFactory.createClass();
            T javassistProxy = (T) proxyClass.newInstance();
            ((ProxyObject) javassistProxy).setHandler((javassist.util.proxy.MethodHandler) handler);
            return javassistProxy;
        }
    });

    private ProxyFactory factory;

    ProxyEnum(ProxyFactory factory) {
        this.factory = factory;
    }

    public <T> T newProxyInstance(Class<T> interfaceType, Object handler) throws Exception {
        return factory.newProxyInstance(interfaceType, handler);
    }

    interface ProxyFactory {
        <T> T newProxyInstance(Class<T> interfaceClass, Object handler) throws Exception;
    }
}
