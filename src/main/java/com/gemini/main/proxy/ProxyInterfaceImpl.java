package com.gemini.main.proxy;

public class ProxyInterfaceImpl implements NeedProxyInterface {
    @Override
    public void doSomething() {
        System.out.println("do something!!!");
    }
}
