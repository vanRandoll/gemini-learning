package com.gemini.main.proxy;

/**
 * gemini
 * com.gemini.main.proxy.ProxyTestService
 *
 * @author zhanghailin
 */
public interface ProxyTestService {

    String test(String s);
}
