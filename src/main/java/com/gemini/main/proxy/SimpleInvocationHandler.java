package com.gemini.main.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class SimpleInvocationHandler implements InvocationHandler {

    private Object target;

    SimpleInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //目标方法前执行
        System.out.println("——————————————————————————");
        System.out.println("下一位请登台发言！");
        //目标方法调用
        Object obj = method.invoke(target, args);
        //目标方法后执行
        System.out.println("大家掌声鼓励！");
        return obj;
    }

    public static void main(String[] args) {
        // 希望被代理的目标业务类
        ProxyInterfaceImpl target = new ProxyInterfaceImpl();
        // 将目标类和横切类编织在一起
        SimpleInvocationHandler handler = new SimpleInvocationHandler(target);
        // 创建代理实例
        NeedProxyInterface proxy = (NeedProxyInterface) Proxy.newProxyInstance(
                target.getClass().getClassLoader(),//目标类的类加载器
                target.getClass().getInterfaces(),//目标类的接口
                handler);//横切类
        proxy.doSomething();
    }
}
