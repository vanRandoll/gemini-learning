package com.gemini.main.proxy;

/**
 * gemini
 * com.gemini.main.proxy.StudentService
 *
 * @author zhanghailin
 */
public class StudentService {

    void study() {
        System.out.println("学生在教师中静悄悄地在学习...");
        rest();
    }

    void rest() {
        System.out.println("学生在教师中休息...");
    }

    public static void main(String[] args) {
        CglibTest cglibTest = new CglibTest();
        //绑定关系，返回的是StudentService的代理对象
        StudentService studentService = (StudentService) cglibTest.getProxy(StudentService.class);
        //执行代理对象的intercept（）方法，而不是StudentService中的study（）方法。
        //CGLIB的具体实现没看，猜测可能使用了多态，enhancer.create() 返回的真实对象的子类，增强的逻辑在子类中。
        studentService.study();
    }
}
