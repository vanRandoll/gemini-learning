package com.gemini.main.rocketmq;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.util.concurrent.atomic.AtomicLong;

public class SyncProducer {

    private static AtomicLong count = new AtomicLong(0);

    public static void main(String[] args) throws Exception {
        //Instantiate with a producer group name.
        DefaultMQProducer producer = new
                DefaultMQProducer("qifeizn-rocketmq-test");
        //设置自动创建topic的key值
        //producer.setCreateTopicKey("AUTO_CREATE_TOPIC_KEY");
        // Specify name server addresses.
        producer.setNamesrvAddr("127.0.0.1:9876");
        //Launch the instance.
        producer.start();
        for (;;) {
            //Create a message instance, specifying topic, tag and message body.
            Message msg = new Message("TopicTest" /* Topic */,
                    "TagA" /* Tag */,
                    "key"+count.incrementAndGet(),
                    ("Hello RocketMQ " +
                            count.get()).getBytes(RemotingHelper.DEFAULT_CHARSET) /* Message body */
            );
            //Call send message to deliver message to one of brokers.
            SendResult sendResult = producer.send(msg);
            System.out.printf("%s%n", sendResult);
            if (count.get() == 100)
                break;
            Thread.sleep(2000);
        }
        //Shut down once the producer instance is not longer in use.
        producer.shutdown();
    }
}
