package com.gemini.main.sort;

public class BubbleSort {

    //原地排序算法 空间复杂度 o(1)
    //稳定排序算法 相同大小的数据在排序前后不会改变顺序
    public void bubbleSort(int[] a) {
        int length;
        if ((length = a.length) == 0) return;

        for (int i = 0; i < length; i++) {
            // 提前退出冒泡循环的标志位
            boolean flag = false;

            for (int j = 0; j < length - i - 1; j++) {
                if (a[j] > a[j + 1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    flag = true; // 标识有数据交换
                }
            }
            if (!flag) break; //没有数据交互，提前退出排序
        }
    }
}
