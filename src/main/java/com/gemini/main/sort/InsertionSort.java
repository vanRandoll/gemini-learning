package com.gemini.main.sort;

import java.util.Arrays;

public class InsertionSort {

    // 插入排序
    public static void insertionSort(int[] a) {
        int length;
        if ((length = a.length) == 0) return;

        for (int i = 1; i < length; i++) {
            int value = a[i], j = i - 1;
            for (; j >= 0; j--) {
                if (a[j] > value) {
                    a[j + 1] = a[j];// 数据移动 交换
                } else {
                    break;// 中断当前for循环
                }
            }
            a[j + 1] = value;//插入数据
        }

    }

    public static void main(String[] args) {
        int[] a = {4,5,6,1,3,2};
        insertionSort(a);
        System.out.println(Arrays.toString(a));
    }
}
