package com.gemini.main.spring;

import com.gemini.main.spring.ioc.MessageService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;

public class App {

    public static void main(String[] args) {
        // 用我们的配置文件来启动一个 ApplicationContext
        /*ApplicationContext context = new ClassPathXmlApplicationContext("classpath:application.xml");

        System.out.println("context 启动成功");

        // 从 context 中取出我们的 Bean，而不是用 new MessageServiceImpl() 这种方式
        MessageService messageService = context.getBean(MessageService.class);
        // 这句将输出: hello world
        System.out.println(messageService.getMessage());*/
        String droneId = "A06-19-00-002";
        int length = droneId.length();
        System.out.println(droneId.substring(length-1)+droneId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        System.out.println(sdf.format(new Date()));
        /*System.out.println(199/200);*/
    }
}
