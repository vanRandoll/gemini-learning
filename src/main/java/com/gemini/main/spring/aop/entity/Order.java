package com.gemini.main.spring.aop.entity;


import lombok.Data;

@Data
public class Order {

    private String username;

    private String product;
}
