package com.gemini.main.spring.aop.entity;

import lombok.Data;

@Data
public class User {

    private String username;

    private Integer age;

}
