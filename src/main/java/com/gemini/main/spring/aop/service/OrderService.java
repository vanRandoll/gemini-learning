package com.gemini.main.spring.aop.service;

import com.gemini.main.spring.aop.entity.Order;

public interface OrderService {

    Order createOrder(String username, String product);

    Order queryOrder(String username);
}
