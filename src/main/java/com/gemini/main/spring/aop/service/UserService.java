package com.gemini.main.spring.aop.service;

import com.gemini.main.spring.aop.entity.User;

public interface UserService {

    User createUser(String username, int age);

    User queryUser();
}
