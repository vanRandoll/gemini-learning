package com.gemini.main.spring.ioc;

public interface MessageService {

    String getMessage();
}
