package com.gemini.main.spring.ioc;

public class MessageServiceImpl implements MessageService {

    @Override
    public String getMessage() {
        return "hello spring!";
    }
}
